FROM snakemake/snakemake

#instal pit
RUN sudo apt-get install git

#install muscle
RUN sudo apt-get update && sudo apt-get install muscle -y

#install mafft
RUN sudo apt-get install mafft -y

#install clustal
RUN sudo apt-get install clustalw -y 

#install biopython
RUN apt-get install python-setuptools python-numpy python-qt4 python-scipy python-mysqldb python-lxml -y
RUN apt-get install python-biopython -y

#install mrbayes

RUN sudo apt-get install mrbayes -y

#install raxml

RUN sudo apt-get install raxml -y

#install phyml

RUN sudo apt-get install phyml -y

#install iq tree

RUN sudo apt-get install iqtree

#run snakefille 


