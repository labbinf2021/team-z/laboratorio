from Bio import SeqIO

import sys

records = list(SeqIO.parse(sys.argv[1], "fasta"))

for idx in range(len(records)):
    records[idx].annotations = {"molecule_type":"DNA"}

#SeqIO.write(aa, "test.nexus", "nexus")

SeqIO.write(records, "test.nexus", "nexus")
# print("Converted %i records" % count)

