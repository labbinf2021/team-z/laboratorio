#!/usr/bin/env python3

from Bio import Phylo
import sys
import os
import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import pylab

import networkx
file_=sys.argv[1]

tree=Phylo.read(file_,'newick')
matplotlib.rc('font', size=6)

fig = plt.figure(figsize=(5, 5), dpi=500)
axes = fig.add_subplot(1, 1, 1)
Phylo.draw(tree, axes=axes)

workdir=os.getcwd()

pylab.savefig('raxml.jpg')
pylab.savefig('raxml.svg')


print("Phylogenetic tree generated and saved as Phylo.jpg and Phylo.svg")
print("Phylogenetic tree generated and saved in directory:%s"%workdir)
